*Still under heavy development.*

**A simple language called lit. Inspired by Lua.**

Example of compiling and running in nodejs.

```javascript
new Lite().compile("say('Hello world')").run();
```