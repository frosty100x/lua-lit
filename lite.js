//Lite v0.1
//Author: Greg Batie

var fs = require('fs');

var Lite = function(){
	var _lite = this;
	var pos = 0;
	this.ast = [];
	
	//Indentifier Exp----
	var IdentifierExp = function(value){ 
		this.type = 'identifier';
		this.value = value;
	}
	IdentifierExp.prototype.getValue = function(context){
		var _var = context.getVar(this.value);
		if(_var == 'undefined')
			throw this.name + " is not a variable.";
		return _var;
	}
	//Func-Exp-----------
	var FuncExp = function(func, isNative){
		this.type = 'function';
		this.func = func;
		this.isNative = isNative;
	}
	FuncExp.prototype.call = function(args){
		if(this.isNative)
			return this.func(args);
	}
	
	//Func-Call----------
	var FuncCall = function(name, args){
		this.name = name;
		this.args = args;
		this.args_values;
	}
	FuncCall.prototype.getValue = function(context){
		var func = context.getVar(this.name);
		if(typeof func == 'undefined' || func.type != 'function')
			throw this.name + " is not a function.";
		var values = [];
		for(var i=0;i<this.args.length;i++)
			values[i] = this.args[i].getValue(context);
		return func.call(values);
	}
	
	//String-Exp---------
	var StringExp = function(value){
		this.type = 'string';
		this.value = value;
	}
	StringExp.prototype.getValue = function(context){ return this; }
	StringExp.prototype.toString = function(){ return this.value; }
	
	//Number Exp---------
	var NumberExp = function(value){
		this.type = 'number';
		this.value = value;
	}
	NumberExp.prototype.getValue = function(context){ return this; }
	NumberExp.prototype.toString = function(){ return this.value.toString(); }
	
	//Assignment Exp-----
	var AssignmentExp = function(assignee, value){
		this.assignee = assignee;
		this.value = value;
	}
	AssignmentExp.prototype.getValue = function(context){
		context.setVar(this.assignee, this.value.getValue(context));
	}
	
	//---Tokenizer-------
	this.tokenizer = function*(){
		var data = _lite.script, char;
		
		function isLetter(){
			return _lite.script[pos].match(/[a-z]/i);
		}
		function isLetterOrNum(){
			return _lite.script[pos].match(/[a-z0-9]/i);
		}
		function isSpace(){
			return _lite.script[pos] == ' ';
		}
		function isTab(){
			return _lite.script[pos] == '\t';
		}
		function isNumber(n){
			return !isNaN(parseFloat(_lite.script[pos])) && isFinite(_lite.script[pos]);
		}
		
		function getWord(){
			var data = _lite.script, st = pos++;
			for(; pos < data.length && isLetterOrNum(data[pos]); pos++);
			return data.substring(st, pos);
		}
		
		function getString(c){
			var data = _lite.script, st = pos++;
			for(; pos <= data.length && data[pos] != c; pos++);
			pos++;
			return data.substring(st+1, pos-1);
		}
		
		function isKeyword(word){
			switch(word){
			case 'set': case 'call':
				return true;
			}
		}
		
		function getNumber(){
			var data = _lite.script, st = pos++;
			for(; pos < data.length && isNumber(data[pos]); pos++);
			return data.substring(st, pos);
		}

		while(pos < data.length){
			char = data[pos];
			if(isLetter()){
				var word = getWord();
				yield {type: isKeyword(word) ? 'keyword' : 'identifier', value: word};
			}
			else if(isNumber()){
				yield {type: 'number', value: parseInt(getNumber())};
			}
			else if(char == '\'' || char == '"'){
				yield {type: 'string', value: getString(char)};
			}
			else if(isSpace()){
				pos++; continue; //yield {type: 'SPACE'};
			}
			else if(char == '\t'){
				pos++; yield {type: 'tab'};
			}
			else if(char == '='){
				pos++; yield {type: 'assign'};
			}
			else if(char == '\n' || char == '\r'){
				pos++; yield {type: 'line_term'};
			}
			else if(char == '('){
				pos++; yield {type: 'punc', value: '('};
			}else if(char == ')'){
				pos++; yield {type: 'punc', value: ')'};
			}else if(char == ','){
				pos++; yield {type: 'punc', value: ','};
			}
			else
				throw "unexpected symbol: " + data[pos];
		}
	}
	
	//Parser-------------
	this.Parser = function(){
		this.handleExp = function(){
			var to = this.current;
			var lastNode = null;
			
			if(to.type == 'line_term')
				this.nextNonNewLine();
			
			if(lastNode = this.handleKeyword())
				return lastNode;
			
			if((lastNode = this.handleTerm()) != null);
			
			if(this.current.type == 'line_term')
				return this.nextNonNewLine();
			
			return this.handleSecondaryExp(lastNode);
		}
		
		this.handleSecondaryExp = function(lastNode){
			var current = this.current;
			var funcObj = null;
			
			if(current.type == 'punc' && current.value == '(' 
				&& lastNode != null && lastNode.type == 'identifier')
				return (funcObj = this.handleCall(lastNode)) == null ? lastNode : funcObj;				
			
			return lastNode;
		}
		
		this.handleTerm = function(){
			var to = this.current;
			
			switch(to.type){
			case 'string':
				this.nextNonNewLine();
				return new StringExp(to.value);
			case 'number':
				this.nextNonNewLine();
				return new NumberExp(parseInt(to.value));
			case 'identifier':
				this.nextNonNewLine();
				return new IdentifierExp(to.value);
				default: return null;
			}
		}
		
		this.handleKeyword = function(){
			switch(this.current.value){
				case 'set': return this.handleSet();
				case 'call': return this.handleCall();
			}
		}
		
		this.handleCall = function(funcIden){
			if(this.InCall) return null;
			this.InCall = true;
			var to, args = [];
			
			this.nextNonNewLine();
			
			while((to = this.current) && to.type != 'eof'){
				var v = to.type
				
				var exp = this.handleExp();
				if(exp != null)
					args.push(exp);
				else this.unexp();
				
				if(this.current.type == 'punc' && this.current.value == ','){
					this.nextNonNewLine();
					continue;
				}
				else if(this.current.type == 'punc' && this.current.value == ')'){
					this.nextNonNewLine(); break;
				}
				
			}
			this.InCall = false;
			return new FuncCall(funcIden.value, args);
		}

		this.handleSet = function(){
			if(this.InAssign) return null;
			this.InAssign = true;
			var to, varname, exp;
			while((to = this.nextNonNewLine()) && to.type != 'eof'){
				if(to.type != 'identifier') this.unexp();
				varname = to.value;
				
				if((to = this.nextNonNewLine().type) != 'assign')
					this.unexp();
				this.nextNonNewLine();
				
				var _exp = this.handleExp();
				if(_exp != null)
					exp = _exp;
				else
					this.unexp()
				break;
			}
			this.InAssign = false;
			return new AssignmentExp(varname, exp);
		}
		
		this.nextNonNewLine = function(){
			do{
				this.next();
			}while(this.current.type == 'line_term')
			return this.current;
		}
		
		this.IsBinOp = function(op){
			switch(op){
			case '-': case '+': case '\\': case '*':
				return true;
			}
			return false;
		}
		
		this.next = function(){
			var to = iter.next();
			return this.current = !to.done ? to.value : {type: 'eof'};
		}

		this.unexp = function(msg){
			throw "Unexpected token: " + JSON.stringify(this.current);
		}
	}
	
	this.Parser.prototype.compile = function(script){
		this.next();
		
		while(this.current.type != 'eof'){
			if(this.current.type == 'line_term') continue;
			
			var exp = this.handleExp();
			if(exp != null)
				_lite.ast.push(exp);
			else{
				if(this.current.type == 'eof') break;
				this.unexp();
			}
		}
	}
	
	//Context-------
	this.Context = function(){
		this.map = {};
		this.getVar = function(name){
			var _var = this.map[name];
			if(_var == undefined)
				throw name + " does not exist."
			return _var;
		}
		this.setVar = function(name, value){
			this.map[name] = value;
		}
	}
	
	this.Context.prototype.NewFunction = function(name, func){
		if(typeof func == 'function')
			this.map[name] = new FuncExp(func, true);
	}
	
	this.Context.prototype.NewString = function(name, value){
		if(typeof value == 'string')
			this.map[name] = new StringExp(value);
	}
	
	var iter = this.tokenizer();
	this.parser = new this.Parser();
	this.context = new this.Context();
	
	//Builtin api-------
	var Api = {};
	Api.say = function(args){
		for(var i=0;i<args.length;i++)
			process.stdout.write(args[i].toString());
	}
	
	Api.len = function(args){
		var counter = 0;
		for(var i=0;i<args.length;i++)
			if(typeof args[i].type == 'string')
				counter += args[i].toString().length;
		return new NumberExp(counter);
	}
	
	Api.readFile = function(args){
		var text;
		try{
			text = fs.readFileSync(args[0].toString(), 'utf8');
		}catch(e){
			text = "<nothing>";
		}
		return new StringExp(text);
	}
	
	this.NewFunction('readFile', Api.readFile);
	this.NewFunction('say', Api.say);
	this.NewFunction('len', Api.len);
	
	this.NewString('version', "Lite v1.0");
}

Lite.prototype.compile = function(script){
	this.script = script;
	this.parser.compile(script);
	return this;
}

Lite.prototype.run = function(){
	var ast = this.ast, last;
	for(var i=0;i<ast.length;i++)
		last = ast[i].getValue(this.context);
	return this;
}

Lite.prototype.NewFunction = function(name, func){
	this.context.NewFunction(name, func);
}

Lite.prototype.NewString = function(name, value){
	this.context.NewString(name, value);
}

var text = fs.readFileSync('./test.lit', 'utf8');
var lt = new Lite();
lt.compile(text).run();